# EasyGraph-data-email-eu
This dataset is preprocessed by XGI

## Source of original data

Source: [email-Eu dataset](https://www.cs.cornell.edu/~arb/data/email-Eu/)

## References

If you use this dataset, please cite these references:

* [Simplicial closure and higher-order link prediction](https://doi.org/10.1073/pnas.1800683115), Austin R. Benson, Rediet Abebe, Michael T. Schaub, Ali Jadbabaie, and Jon Kleinberg. Proceedings of the National Academy of Sciences (PNAS), 2018.
* [Local Higher-order Graph Clustering](https://doi.org/10.1145/3097983.3098069), Hao Yin, Austin R. Benson, Jure Leskovec, and David F. Gleich. Proceedings of KDD, 2017.
* [Graph Evolution: Densification and Shrinking Diameters](https://doi.org/10.1145/1217299.1217301), Jure Leskovec, Jon Kleinberg, and Christos Faloutsos. ACM Transactions on Knowledge Discovery from Data, 2007.
